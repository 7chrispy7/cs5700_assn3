from unittest import TestCase
from Triangle import Triangle
from PIL import Image, ImageDraw
import math
import os


class TestTriangle(TestCase):
    def test_scale(self):
        triangle = Triangle(0, 0, 0, 1, 1, 1)
        triangle.scale(2)
        test = Triangle(0, 0, 0, 2, 2, 2)
        assert(triangle.origin == test.origin and triangle.point1 == test.point1 and triangle.point2 == test.point2)

    def test_translate(self):
        triangle = Triangle(0, 0, 0, 1, 1, 1)
        triangle.translate(1, 1)
        test = Triangle(1, 1, 1, 2, 2, 2)
        assert (triangle.origin == test.origin and triangle.point1 == test.point1 and triangle.point2 == test.point2)

    def test_get_area(self):
        triangle = Triangle(0, 0, 0, 3, 3, 3)
        test = triangle.get_area()
        test2 = 3*math.sqrt(2)
        assert(triangle.get_area() == (3 * 3) / 2.0)

    def test_save(self):
        os.chdir('..')
        triangle = Triangle(0, 0, 0, 1, 1, 1)
        triangle.save('TestObjects/rect.txt')
        f = open('Objects/TestObjects/rect.txt')
        test_string = f.read()
        assert (test_string == 'Shape,Triangle,0,0,0,1,1,1')

    def test_add_to_canvas(self):
        img = Image.new('RGB', (100, 100), (255, 255, 255))
        canvas = ImageDraw.Draw(img)
        triangle = Triangle(0, 0, 0, 1, 1, 1)
        assert (triangle.add_to_canvas(canvas) == canvas)

    def test_check_frame(self):
        triangle = Triangle(-10, -10, -10, -5, -5, -5)
        triangle.check_frame()
        test = Triangle(0, 0, 0, 5, 5, 5)
        assert (triangle.origin == test.origin and triangle.point1 == test.point1 and triangle.point2 == test.point2)
