from PIL import Image, ImageDraw
from Shape import Shape
import math


class Circle(Shape):
    origin = (0, 0)
    radius = 0

    def __init__(self, x, y, r):
        self.origin = (x, y)
        self.radius = r

    def scale(self, factor):
        self.radius *= factor

    def translate(self, x, y):
        new_x = max(self.origin[0] + x, 0)
        new_y = max(self.origin[1] + y, 0)
        self.origin = (new_x, new_y)
        return self

    def get_area(self):
        return self.radius * self.radius * math.pi

    def save(self, filename):
        f = open('Objects/' + filename, 'w+')
        f.write(self.get_string())
        f.close()

    def render(self, filename):
        img = Image.new('RGB', (max(100, self.origin[0] + 1), max(100, self.origin[1] + 1)), (255, 255, 255))
        draw = ImageDraw.Draw(img)
        self.add_to_canvas(draw)
        img.save('Images/' + filename)

    def add_to_canvas(self, canvas):
        canvas.ellipse([self.origin[0] - self.radius, self.origin[1] - self.radius,
                        self.origin[0] + self.radius, self.origin[1] + self.radius], outline='black')
        return canvas

    def get_string(self):
        return 'Shape,Circle,' + str(self.origin[0]) + ',' + str(self.origin[1]) + ',' + str(self.radius)
