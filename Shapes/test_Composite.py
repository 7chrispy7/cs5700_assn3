from unittest import TestCase
from Composite import Composite
from Circle import Circle
from Rectangle import Rectangle
import math


class TestComposite(TestCase):
    def test_scale(self):
        c = Composite()
        circle = Circle(5, 5, 6)
        c.add_shape(circle)
        c.scale(2)
        assert(circle.radius == 12 and circle.origin == (5, 5))

    def test_translate(self):
        c = Composite()
        circle = Circle(5, 5, 6)
        c.add_shape(circle)
        c.translate(3, 3)
        assert(circle.radius == 6 and circle.origin == (8, 8))

    def test_get_area(self):
        c = Composite()
        circle = Circle(5, 5, 6)
        c.add_shape(circle)
        assert(c.get_area() == 36 * math.pi)

    def test_get_string(self):
        c = Composite()
        circle = Circle(5, 5, 6)
        c.add_shape(circle)
        c.add_shape(Rectangle(0, 0, 5, 5))
        c1 = Composite()
        c1.add_shape(Circle(2,2,2))
        c.add_shape(c1)
        assert(c.get_string() == 'Shape,Composite(Shape,Circle,5,5,6)(Shape,Rectangle,0,0,5,5)(Shape,Composite(Shape,Circle,2,2,2))')
