from unittest import TestCase
from Point import Point
from PIL import Image, ImageDraw
import os


class TestPoint(TestCase):
    def test_scale(self):
        p = Point(1,1)
        test = p.scale(1)
        assert (p == test)

    def test_translate(self):
        p = Point(1,1)
        p.translate(1, 1)
        assert(p.origin == (2, 2))

    def test_get_area(self):
        p = Point(1,1)
        assert(p.get_area() == 0)

    def test_save(self):
        os.chdir('..')
        p = Point(1,1)
        p.save('TestObjects/point.txt')
        f = open('Objects/TestObjects/point.txt')
        test_string = f.read()
        assert(test_string == 'Shape,Point,1,1')

    def test_add_to_canvas(self):
        img = Image.new('RGB', (100, 100), (255, 255, 255))
        canvas = ImageDraw.Draw(img)
        p = Point(1, 1)
        assert (p.add_to_canvas(canvas) == canvas)
