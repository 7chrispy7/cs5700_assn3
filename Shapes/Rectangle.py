from Shape import Shape
from PIL import Image, ImageDraw


class Rectangle(Shape):
    origin = (0, 0)
    point1 = (0, 0)

    def __init__(self, x1, y1, x2, y2):
        self.origin = (x1, y1)
        self.point1 = (x2, y2)
        self.check_frame()

    def scale(self, factor):
        delta_x = self.point1[0] - self.origin[0]
        delta_y = self.point1[1] - self.origin[1]
        delta_x *= factor
        delta_y *= factor
        self.point1 = (self.origin[0] + delta_x, self.origin[1] + delta_y)
        self.check_frame()
        return self

    def translate(self, x, y):
        new_x1 = self.origin[0] + x
        new_y1 = self.origin[1] + y
        new_x2 = self.point1[0] + x
        new_y2 = self.point1[1] + y
        self.origin = (new_x1, new_y1)
        self.point1 = (new_x2, new_y2)
        self.check_frame()
        return self

    def get_area(self):
        return abs((self.origin[0] - self.point1[0]) * (self.origin[1] - self.point1[1]))

    def save(self, filename):
        f = open('Objects/' + filename, 'w+')
        f.write(self.get_string())
        f.close()

    def render(self, filename):
        img = Image.new('RGB',
                        (max(100, self.origin[0] + 1, self.point1[0]), max(100, self.origin[1] + 1, self.point1[1])),
                        (255, 255, 255))
        draw = ImageDraw.Draw(img)
        self.add_to_canvas(draw)
        img.save('Images/' + filename)

    def add_to_canvas(self, canvas):
        canvas.rectangle([self.origin[0], self.origin[1], self.point1[0], self.point1[1]], outline='black')
        return canvas

    def check_frame(self):
        if self.origin[0] < 0:
            self.translate(-self.origin[0], 0)
        if self.origin[1] < 0:
            self.translate(0, -self.origin[1])
        if self.point1[0] < 0:
            self.translate(-self.point1[0], 0)
        if self.point1[1] < 0:
            self.translate(0, -self.point1[1])

    def get_string(self):
        return 'Shape,Rectangle,' + str(self.origin[0]) + ',' + str(self.origin[1]) + \
                ',' + str(self.point1[0]) + ',' + str(self.point1[1])
