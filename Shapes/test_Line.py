from unittest import TestCase
from Line import Line
from PIL import Image, ImageDraw
import os
import math


class TestLine(TestCase):
    def test_scale(self):
        line = Line(0, 0, 1, 1)
        line.scale(2)
        test = Line(0, 0, 2, 2)
        assert(line.origin == test.origin and line.point1 == test.point1)

    def test_translate(self):
        line = Line(0, 0, 1, 1)
        line.translate(1, 1)
        test = Line(1, 1, 2, 2)
        assert(line.origin == test.origin and line.point1 == test.point1)

    def test_get_area(self):
        line = Line(0, 0, 1, 1)
        assert(line.get_area() == 0)

    def test_get_length(self):
        line = Line(0, 0, 1, 1)
        length = line.get_length()
        assert(length == math.sqrt(2))

    def test_save(self):
        os.chdir('..')
        line = Line(0, 0, 1, 1)
        line.save('TestObjects/line.txt')
        f = open('Objects/TestObjects/line.txt')
        test_string = f.read()
        assert (test_string == 'Shape,Line,0,0,1,1')

    def test_add_to_canvas(self):
        img = Image.new('RGB', (100, 100), (255, 255, 255))
        canvas = ImageDraw.Draw(img)
        line = Line(0, 0, 1, 1)
        assert (line.add_to_canvas(canvas) == canvas)

    def test_check_frame(self):
        line = Line(-10, -10, -9, -9)
        line.check_frame()
        test = Line(0, 0, 1, 1)
        assert (line.origin == test.origin and line.point1 == test.point1)
