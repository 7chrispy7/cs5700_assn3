from PIL import Image, ImageDraw


class Shape:
    origin = (0, 0)

    def __init__(self):
        pass

    def scale(self, factor):
        return self

    def translate(self, x, y):
        new_x = max(self.origin[0] + x, 0)
        new_y = max(self.origin[1] + y, 0)
        self.origin = (new_x, new_y)
        return self

    def get_area(self):
        return 0

    def save(self, filename):
        f = open('Objects/' + filename, 'w+')
        f.write(self.get_string())
        f.close()

    def render(self, filename):
        img = Image.new('RGB', (100, 100), (255, 255, 255))
        # draw = ImageDraw.Draw(img)  # This line is not needed for the parent shape.
        img.save('Images/' + filename)

    def add_to_canvas(self, canvas):
        return canvas

    def get_string(self):
        return 'Shape,None,' + str(self.origin[0]) + ',' + str(self.origin[1])
