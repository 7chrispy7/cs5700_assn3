from PIL import Image

class ImageFlyweight:
    images = []

    def __init__(self):
        pass

    def add_to_canvas(self, img_origin, img_size, img_name, canvas):
        for image in self.images:
            if image.filename == img_name:
                canvas.bitmap([img_origin[1], img_origin[2]], image.resize(img_size))
        return canvas

    def add_image(self, filename):
        has_img = False
        for img in self.images:
            if img.filename == filename:
                has_img = True
        if not has_img:
            self.images.append(Image.open(filename))