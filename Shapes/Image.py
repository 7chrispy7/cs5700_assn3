from Shape import Shape
from PIL import Image as ImagePIL
from PIL import ImageDraw


class Image(Shape):
    origin = (0, 0)
    size = (0, 0)
    image_name = ''
    flyweight = None

    def __init__(self, image_name, x, y, x_size, y_size, flyweight):
        self.image_name = image_name
        self.origin = (x, y)
        self.size = (x_size, y_size)
        self.flyweight = flyweight

    def scale(self, factor):
        self.size = (self.size[0] * factor, self.size[1] * factor)
        return self

    def translate(self, x, y):
        new_x = max(self.origin[0] + x, 0)
        new_y = max(self.origin[1] + y, 0)
        self.origin = (new_x, new_y)
        return self

    def get_area(self):
        return self.size[0] * self.size[1]

    def save(self, filename):
        f = open('Objects/' + filename, 'w+')
        f.write(self.get_string())
        f.close()

    def render(self, filename):
        img = ImagePIL.new('RGB',
                           (max(100, self.origin[0] + self.size[0]), max(100, self.origin[1] + self.size[1])),
                           (255, 255, 255))
        canvas = ImageDraw.Draw(img)
        self.add_to_canvas(canvas)
        img.save('Images/' + filename)

    def add_to_canvas(self, canvas):
        return self.flyweight.add_to_canvas(self.origin, self.size, self.image_name, canvas)

    def get_string(self):
        return 'Shape,Image,' + str(self.origin[0]) + ',' + str(self.origin[1]) + \
                ',' + str(self.size[0]) + ',' + str(self.size[1]) + ',' + self.image_name
