from Shapes import Point, Line, Rectangle, Circle, Triangle, Image, ImageFlyweight, Composite


class ShapeFactory:
    flyweight = None

    def make_shape(self, input_string):
        shape_string = input_string.split(',')
        if shape_string[0] != 'Shape':
            return False
        elif shape_string[1] == 'Point':
            return Point.Point(shape_string[2], shape_string[3])
        elif shape_string[1] == 'Line':
            return Line.Line(shape_string[2], shape_string[3], shape_string[4], shape_string[5])
        elif shape_string[1] == 'Rectangle':
            return Rectangle.Rectangle(shape_string[2], shape_string[3], shape_string[4], shape_string[5])
        elif shape_string[1] == 'Circle':
            return Circle.Circle(shape_string[2], shape_string[3], shape_string[4])
        elif shape_string[1] == 'Triangle':
            return Triangle.Triangle(shape_string[2], shape_string[3], shape_string[4], shape_string[5],
                                     shape_string[6], shape_string[7])
        elif shape_string[1] == 'Image':
            return self.make_image(shape_string)
        elif shape_string[1] == 'Composite':
            return self.make_composite(input_string)

    def make_image(self, image_string):
        if self.flyweight is None:
            self.flyweight = ImageFlyweight()
        self.flyweight.add_image(image_string[6])
        return Image.Image(image_string[6], image_string[2], image_string[3],
                           image_string[4], image_string[5], self.flyweight)

    def make_composite(self, composite_string):
        composite_string = self.parse_string(composite_string)
        composite = Composite.Composite()
        for shape in composite_string:
            composite.add_shape(shape)
        return composite

    def parse_string(self, string):
        paren_depth = 0;
        string_list = []
        next_str = ''
        for char in string:
            if char == ')':
                paren_depth -= 1
                if paren_depth == 0:
                    string_list.append(next_str)
                    next_str = ''
            if paren_depth > 0:
                next_str += char
            if char == '(':
                paren_depth += 1
        return string_list

